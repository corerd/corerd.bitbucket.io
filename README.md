# corerd.bitbucket.io

Navigate to [corerd website](https://corerd.bitbucket.io/)


# Debug with Simple HTTP request handler

	python2 -m SimpleHTTPServer [<port-number>]
	python3 -m http.server [<port-number>]

default port-number is 8000.


# Convert Markdown to HTML

	python markdown2.py foo.md > foo.html

Check out [markdown2](https://github.com/trentm/python-markdown2).
